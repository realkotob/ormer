tool

extends StaticBody2D

signal hexa_pressed

export (float, 0, 100) var radius = 1
export (String) var _text = "BUTTON" setget text
export (bool) var horizontal = false
export (bool) var disabled = false setget disable
export (bool) var selectable = true
export (bool) var selected = false setget select
export (bool) var _generate = false setget generate

export (Dictionary) var test_style = null

export (Vector2) var start_scale = Vector2(1,1)

export (Color) var normal_bg_color = Color(1,1,1,1)
export (Color) var normal_txt_color = Color(0,0,0,1)
export (Vector2) var normal_scale = Vector2(1,1)
export (float, 0, 100) var normal_speed = 10

export (Color) var hover_bg_color = Color(1,0,1,1)
export (Color) var hover_txt_color = Color(0,1,1,1)
export (Vector2) var hover_scale = Vector2(0.9,0.9)
export (float, 0, 100) var hover_speed = 10

export (Color) var selected_bg_color = Color(1,0,1,1)
export (Color) var selected_txt_color = Color(0,1,1,1)
export (Vector2) var selected_scale = Vector2(0.9,0.9)
export (float, 0, 100) var selected_speed = 10

export (Color) var disabled_bg_color = Color(1,0,1,1)
export (Color) var disabled_txt_color = Color(0,1,1,1)
export (Vector2) var disabled_scale = Vector2(0.8,0.8)
export (float, 0, 100) var disabled_speed = 10

onready var init_txt = _text
onready var target_bg_color = normal_bg_color
onready var target_txt_color = normal_txt_color
onready var target_scale = normal_scale
onready var speed = normal_speed

func text( t ):
	if t == null:
		if init_txt == null:
			init_txt = "blank"
		t = init_txt
	_text = t
	if $txt != null:
		$txt.text = _text

func select( b ):
	selected = b
	if selected:
		target_bg_color = selected_bg_color
		target_txt_color = selected_txt_color
		target_scale = selected_scale
		speed = selected_speed
	else:
		target_bg_color = normal_bg_color
		target_txt_color = normal_txt_color
		target_scale = normal_scale
		speed = normal_speed

func disable( b ):
	disabled = b
	if disabled:
		target_bg_color = disabled_bg_color
		target_txt_color = disabled_txt_color
		target_scale = disabled_scale
		speed = disabled_speed
	else:
		select( selected )

func generate( b ):
	_generate = false
	disable( disabled )
	if b:
		var pts = PoolVector2Array()
		var start_at = 1
		if horizontal:
			start_at = 0
		for i in range(start_at,12,2):
			var a = TAU / 12 * i
			var v2 = Vector2( cos(a) * radius, sin(a) * radius )
			pts.append( v2 )
		$mesh.polygon = pts
		$coll.polygon = pts
		$txt.rect_size = Vector2( radius * 2, $txt.rect_size.y)
		$txt.rect_position = Vector2( -radius, $txt.rect_size.y * -0.5 )
		# setting text
		text( _text )
		# setting colors
		$mesh.color = normal_bg_color
		$txt.set( "custom_colors/font_color", normal_txt_color )

func new_style():
	return {
		'bg' : Color(1,1,1,1),
		'txt' : Color(0,0,0,1),
		'scale' : Vector2(1,1),
		'speed' : 10
	}

func cp_style( template ):
	start_scale = template.start_scale
	normal_bg_color = template.normal_bg_color
	normal_txt_color = template.normal_txt_color
	normal_scale = template.normal_scale
	normal_speed = template.normal_speed
	hover_bg_color = template.hover_bg_color
	hover_txt_color = template.hover_txt_color
	hover_scale = template.hover_scale
	hover_speed = template.hover_speed
	disabled_bg_color = template.disabled_bg_color
	disabled_txt_color = template.disabled_txt_color
	disabled_scale = template.disabled_scale
	disabled_speed = template.disabled_speed

func restart():
	$mesh.scale = start_scale

func _ready():
	disable( disabled )
	restart()
	$mesh.color = target_bg_color
	$txt.set( "custom_colors/font_color", target_txt_color )
	pass

func _process(delta):
	
	if test_style == null:
		test_style = new_style()
	
	if delta < 0.03 and $mesh.scale != target_scale:
		$mesh.scale += ( target_scale - $mesh.scale ) * delta * speed
		$mesh.color += ( target_bg_color - $mesh.color ) * delta * speed
		var tc = $txt.get( "custom_colors/font_color" )
		$txt.set(  "custom_colors/font_color", tc + ( target_txt_color - tc ) * delta * speed )
		if ( $mesh.scale - target_scale ).length() < 0.01:
			$mesh.scale = target_scale
			$mesh.color = target_bg_color
			$txt.set(  "custom_colors/font_color", target_txt_color )
	pass

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == 1 and not disabled:
			emit_signal( "hexa_pressed", self )
			if selectable:
				select( !selected )

func _on_mouse_entered():
	if disabled or selected:
		return
	target_bg_color = hover_bg_color
	target_txt_color = hover_txt_color
	target_scale = hover_scale
	speed = hover_speed
	pass

func _on_mouse_exited():
	if disabled or selected:
		return
	target_bg_color = normal_bg_color
	target_txt_color = normal_txt_color
	target_scale = normal_scale
	speed = normal_speed
	pass
