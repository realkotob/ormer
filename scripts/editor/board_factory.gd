tool

extends Spatial

export (int,0,10) var depth_min = 1
export (int,0,10) var depth_max = 5
export (bool) var _generate = false setget generate

var board_tmpl_path = "res://models/board.tscn"
var board_target_folder = "res://models/boards/"

func purge():
	while get_child_count() > 0:
		remove_child( get_child(0) )

func recursive_owner( node, new_owner, lvl, max_lvl ):
	if lvl >= max_lvl:
		print( "recursive_owner, too deep: ", lvl, " <> ", max_lvl )
		return
	if not node == new_owner and (not node.owner or node.filename):
		node.set_owner( new_owner )
	lvl += 1
	for c in node.get_children():
		c.set_owner( new_owner )
		recursive_owner( c, new_owner, lvl, max_lvl )

func generate(b):
	
	_generate = false
	
	if b:
		
		if depth_max < depth_min:
			depth_max = depth_min
		
		for i in range( depth_min, depth_max + 1 ):
			
			purge()
	
			var board = load(board_tmpl_path).instance()
			board.depth = i
			add_child( board )
			board.do_generate()
#			board.set_owner( get_tree().get_edited_scene_root() )
			recursive_owner( board, board, 0, 2 )
			
			var path = str( i )
			while len( path ) < 3:
				path = "0" + path
			path = board_target_folder + "board_" + path + ".tscn"
		
			var ps = PackedScene.new()
			var result = ps.pack(board)
			if result == OK:
		# warning-ignore:return_value_discarded
				ResourceSaver.save(path, ps)
				print( "board saved at " + path )

func _ready():
	purge()