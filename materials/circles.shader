shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,unshaded;
uniform vec4 albedo : hint_color;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;
uniform float speed;
uniform float frequence;
uniform float thickness;
uniform float gradient;
uniform float moire;

void vertex() {
	UV = UV * uv1_scale.xy + uv1_offset.xy; // + vec2(TIME,0)*speed;
}

void fragment() {
	
	vec2 base_uv = UV;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	SPECULAR = 0.0;
	
	float freq = sqrt( UV.x*UV.x + UV.y*UV.y ) * cos( UV.x * moire ) * cos( UV.y * moire );
	float cmult = ( 1.0 + sin( freq * frequence + TIME * speed ) ) * 0.5;
	cmult = min(1.0, max( 0.0, cmult - ( 0.5 - gradient * thickness )) / gradient );
	ALBEDO = mix( albedo.rgb, vec3(1.0,1.0,1.0) - albedo.rgb, cmult );
	ALPHA = albedo.a;
	
//	ALBEDO = albedo.rgb;
//	int d = int( ( sqrt( UV.x*UV.x*UV.x*UV.x + UV.y*UV.y*UV.y*UV.y ) ) * 1000.0 );
//	int dd = ( d + int( TIME * 1000.0 * speed ) ) % 30;
//	if ( d < int( 1000.0 / 2.05 ) && dd < 15 ) {
//		ALPHA = 1.0;
//		ALBEDO = vec3(1.0,1.0,1.0) - albedo.rgb;
//	} else {
//		ALPHA = albedo.a;
//	}

}
