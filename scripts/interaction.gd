extends Node

signal pastille_hover
signal pastille_click

const ray_length = 1000
const v_up = Vector3(0,1,0)
const v_front = Vector3(0,0,1)

var ray_process = false
var drag_mouse_pos = Vector2()
var mouse_pos = Vector2()
var dragged = null
var drag_init = null
var pastille_hover = null
var pusher_mode = 0

var input_enabled = true
var pastilles_enabled = false

onready var cam = $cam_pivot/cam

func _ready():
	$pusher.visible = false
	move_pusher( Vector3(0,-1000,0) )

# warning-ignore:unused_argument
func _physics_process(delta):
	
	mouse_pos = get_viewport().get_mouse_position()
	
	if ray_process:
		ray_process = false
		var ray = $hit_rc
		if ray.is_colliding() and ray.get_collider() is RigidBody:
			dragged = ray.get_collider()
			if dragged.has_method( "highlight" ):
				dragged.highlight(true)
			drag_init = dragged.global_transform.origin
			drag_mouse_pos = mouse_pos
			enable_pusher( drag_init )
			$pusher.visible = false
		else:
			release_dragged()
	
	elif dragged != null:

		var do = dragged.global_transform.origin
		
		var cam_pos = cam.project_ray_origin(mouse_pos)
		var cam_norm = cam.project_ray_normal(mouse_pos)
		var mult = cam_norm.dot( Vector3(0,-1,0) )
		var displace = ( cam_norm * (cam_pos.y - drag_init.y) ) / mult
		var dp = cam_pos + displace
		
		var vl = (dp-do).length()
		if vl > 0.05:
			orient_pusher( dp )
			$pusher.visible = true
		if vl >= 1:
			pusher_mode = 1
		if pusher_mode == 1:
			move_pusher( dp )

#####################
# physics management #
#####################

func release_dragged():
	if dragged != null and dragged.has_method( "highlight" ):
		dragged.highlight(false)
	disable_pusher()
	dragged = null

func update_ray( ray, v2 ):
	ray.translation = cam.project_ray_origin(v2)
	ray.cast_to = ray.translation + cam.project_ray_normal(v2) * ray_length

func disable_pusher():
	pusher_mode = 0
	$pusher.visible = false
	$pusher.set_collision_layer_bit( 0, false )
	$pusher.set_collision_mask_bit( 0, false )

func enable_pusher( v3 ):
	pusher_mode = 0
	$pusher.visible = true
	$pusher.set_collision_layer_bit( 0, true )
	$pusher.set_collision_mask_bit( 0, true )
	$pusher.translation = v3
	if dragged != null and dragged.has_method( "get_albedo" ):
		$pusher.set_mat_albedo( dragged.get_albedo() )

func orient_pusher( v3 ):
	$pusher.look_at( v3, v_up )

func move_pusher( v3 ):
	var offset = $pusher.global_transform.basis.xform( v_front ) * -1
	$pusher.translation += ( ( v3 - offset ) - $pusher.translation ) * 0.3

#####################
# events management #
#####################

func _input(event):
	
	if not input_enabled:
		pastille_hover = null
		emit_signal( "pastille_hover", pastille_hover )
		ray_process = false
		$cam_pivot.enabled = false
		return
	else:
		$cam_pivot.enabled = true
	
	if event is InputEventMouseButton:
		
		if pastille_hover != null and event.button_index == 1 and event.pressed:
			emit_signal( "pastille_click", pastille_hover )
			pass
		
		elif event.pressed and event.button_index == 1:
			ray_process = true
			update_ray( $hit_rc, event.position )
		
		elif not event.pressed and event.button_index == 1:
			if dragged != null:
				release_dragged()
	
	elif event is InputEventMouseMotion and pastilles_enabled:
		update_ray( $button_rc, event.position )
		if $button_rc.is_colliding():
			var b = $button_rc.get_collider()
			pastille_hover = b
			emit_signal( "pastille_hover", pastille_hover )
		else:
			pastille_hover = null
			emit_signal( "pastille_hover", pastille_hover )